package com.cbalcazar.pragma.mc.image.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class ImageController {

    @GetMapping("/")
    String helloFromMCImage() {
        return "helloFromMCImage";
    }

}
